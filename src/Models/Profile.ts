export class Profile{
    id: number;

    name: string;

    bio: string;

    photo_file: string;
}