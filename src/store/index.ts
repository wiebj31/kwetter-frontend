import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    trendtweets: [],
    populartrends: [],
    trendTweetOverlay: false
  },
  getters:{
    populartrends: (state) => {
      return state.populartrends;
    }
  },
  mutations: {
    trendtweets(state, payload){
      state.trendtweets = payload
    },
    populartrends(state, payload){
      state.populartrends = payload
    },
    trendTweetOverlay(state, payload){
      state.trendTweetOverlay = !state.trendTweetOverlay
    }
  },
  actions: {},
  modules: {}
});
