import { authGuard } from "@/auth/authGuard";
import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Profile from "../views/Profile.vue";
import Userlist from "../views/Userlist.vue"
import LandingPage from "../views/LandingPage.vue"

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "LandingPage",
    component: LandingPage
  },  
  {
    path: "/home",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
    beforeEnter:authGuard
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
    beforeEnter:authGuard
  },
  {
    path: "/userlist",
    name: "userlist",
    component: Userlist,
    beforeEnter:authGuard
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});


export default router;
