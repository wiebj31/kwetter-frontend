import AxiosRequestHandler from '@/utils/AxiosRequestHandler';

class ProjectService{
    public static getAllProfiles(token: string): any{
        return AxiosRequestHandler.get('/profiles', token);
    }

    public static GetById(token: string, Id:string): any{
        return AxiosRequestHandler.get(`/profile/${Id}`, token);
    }

    public static createProfile(token: string, userId: object){
        return AxiosRequestHandler.post('/profile', userId, token);
    }

    public static updateBio(token: string, bio: object){
        return AxiosRequestHandler.patch('/update', bio, token)
    }
}

export default ProjectService;