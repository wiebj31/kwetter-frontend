import AxiosRequestHandler from '@/utils/AxiosRequestHandler';

class TweetService{
    public static getAllTweets(token: string, pagen: number): any{
        return AxiosRequestHandler.get(`/tweets/${pagen}`, token);
    }

    public static GetById(token: string, Id:string): any{
        return AxiosRequestHandler.get(`/profile/${Id}`, token);
    }

    public static createTweet(token: string, tweet: object){
        return AxiosRequestHandler.post('/tweet', tweet, token);
    }

    public static GetByProfileId(token: string, pagen: number, profileId: string){
        return AxiosRequestHandler.get(`/tweets/${profileId}/${pagen}`, token);
    }

    public static getByTrend(token: string, trendname: string){
        return AxiosRequestHandler.get(`/getbytrend/${trendname}`, token);
    }


    // public static updateBio(token: string, bio: object){
    //     return AxiosRequestHandler.patch('/update', bio, token)
    // }
}

export default TweetService;