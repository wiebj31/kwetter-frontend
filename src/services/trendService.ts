import AxiosRequestHandler from '@/utils/AxiosRequestHandler';

class TrendService{
    //TO DO Create get all trends
    // public static getAllProfiles(token: string): any{
    //     return AxiosRequestHandler.get('/profiles', token);
    // }

    public static GetByName(token: string, name:string): Promise<any>{
        return AxiosRequestHandler.get(`/trend/${name}`, token);
    }

    public static checkForTrend(token: string, trend: object){
        return AxiosRequestHandler.post('/trend/check', trend, token);
    }

    public static getPopular(token: string){
        return AxiosRequestHandler.get('/trend_popular', token);
    }
}

export default TrendService;